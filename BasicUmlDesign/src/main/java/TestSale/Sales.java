
package TestSale;

import ventas.*;

public class Sales {
    public static void main(String[] args) {
        //creamos varios objetos de tipo producto
        Product product1 = new Product("Camisa", 50);
        Product product2 = new Product("Pantalon", 100);
        
        //creamos un objeto de tipo orden
        Orden orden1 = new Orden();
        //agregamos los productos a la orden
        orden1.addProduct(product1);
        orden1.addProduct(product2);
        
        //imprimimos la orden
        orden1.showOrden();
    }
}
