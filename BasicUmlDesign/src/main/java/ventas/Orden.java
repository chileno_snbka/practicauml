package ventas;

import java.util.Calendar;
import java.util.Date;

public class Orden {

    private Date date;
    private int idOrden;
    private Product products[]; //se declara la variable arreglo
    private static int countOrdens;
    private int countProducts;
    private static final int MAX_PRODUCTS = 10;

    public Orden() {
        this.idOrden = ++countOrdens;
        //se inicializa el arreglo
        products = new Product[MAX_PRODUCTS];
    }

    public void addProduct(Product product) {
        if (this.countProducts < MAX_PRODUCTS) {
            products[this.countProducts++] = product; //++ para ir moviendo el índice al agregar un producto
        } else {
            System.out.println("Se ha superado el máximo de productos: " + MAX_PRODUCTS);
        }
    }

    public double calculateTotal() {
        //sumamos el atributo precio
        double total = 0;
        for (int i = 0; i < this.countProducts; i++) {
            Product product = this.products[i];
            total += product.getPrice(); //total = total + product.getPrice()
        }
        return total;
    }

    public void showOrden() {
        System.out.println("Fecha de la orden: " + this.getDate());
        System.out.println("Orden N°: " + this.idOrden);
        System.out.println("Total de la orden: " + this.calculateTotal());
        System.out.println("Productos de la orden: ");
        for (int i = 0; i < this.countProducts; i++) {
            System.out.println(products[i]);
        }
    }

    public Date getDate() {
        date = new Date();
        return date;
    }
}
