package ventas;

public class Product {
    private int idProduct;
    private String name;
    private double price;
    private static int countProduct;
    
    //Constructor vacío
    private Product() {
        this.idProduct = ++countProduct;
    }
    
    //Constructor sobrecargado de 2 argumentos
    public Product(String name, double price) {
        this();
        this.name = name;
        this.price = price;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public static int getCountProduct() {
        return countProduct;
    }

    @Override
    public String toString() {
        return "Product{" + "idProduct=" + idProduct + ", name=" + name + ", price=" + price + '}';
    }
}
